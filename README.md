# Задания по курсу Технологии и практики MLOps
В данном репозитории содержатся материалы для развёртки MLFlow для основного проекта.

## Команда
Роман Одобеску, Роман Сим

## Описание
В рамках инфраструктуры для проведеняи экспериментов у нас на удаленном сервере развёрнуты: Minio, MLFlow, Postges DB, PGAdmin.   

В Minio хранятся все результаты экспериментов, логируемые через MLFLow, также там хранятся модели в MLlow Model Registry и версионированнные через DVC данные:   

![alt text](./images/buckets.png)

![alt text](./images/dvc.png)

![alt text](./images/dvc_mlflow.png)

MLFlow служит для трекинга экспериментов, данные по экспериментам записываются в postgres, а артефакты - в минио. 

![alt text](./images/mlflow_experiments.png)
![alt text](./images/experiment_result.png)

Все сервисы описаны в docker-compose.yaml. Для того чтобы запустить это, вам потребуется создать .env файл наподобие .env_example, представленного здесь, и заполнить все значения. Учтите, что на стороне клиента MLFlow (т.е. там, где будут запускаться эксперименты), также должен быть данный файл. Сервисы можно запустить с помощью команды:
```bash
docker-compose up --build
```

Помимо этих сервисов, также у нас работают сервис модели на FastAPI, Nexus, а также Prometeus и Grafana для мониторинга. Их описание вы можете найти в https://gitlab.com/roman_odobesku/mlops_itmo_serving